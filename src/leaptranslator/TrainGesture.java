/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package leaptranslator;

import com.leapmotion.leap.Controller;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Brett Van Zijl
 */
public class TrainGesture {
    
    public void run() throws IOException, InterruptedException{
       System.out.println("Press Enter New Gesture Name:");
       Scanner input = new Scanner(System.in);
       String name = input.nextLine();
       
       int countDown = 3;
       while (countDown>0){
           System.out.println("Recording Gesture in ... " + Integer.toString(countDown));
           countDown--;
           Thread.sleep(1000);
       }
       
       Trainer listener = new Trainer(name);
       Controller controller = new Controller();
       controller.setPolicy(Controller.PolicyFlag.POLICY_OPTIMIZE_HMD);//optimized for head mounted display (LM being in vertical orientation)
       controller.addListener(listener);
       
     /* while (!listener.stopRecording){
          //System.out.println("Recording..." + listener.stopRecording.toString());
       }*/
        
       // Keep this process running until Enter is pressed
       //This is the only method that I could find that allows the process to run without causing issues. while(true) statements did not work.
        try {

           System.in.read();
       } catch (IOException e) {
           e.printStackTrace();
       }
        
       controller.removeListener(listener);
       System.out.println("Removed Listener");
       String fileName= "Gestures\\" + listener.gesture.name;
       ObjectInputStream fInput = null;
       Gesture gesture=null;
       
      try{
            fInput = new ObjectInputStream(new BufferedInputStream(new FileInputStream(fileName)));
            gesture = (Gesture) fInput.readObject();
       
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(LeapTranslator.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            fInput.close();
        }
       
       System.out.println("Name: " + gesture.name);
       System.out.println("Points: " + gesture.points.toString());
    }   
    
}


