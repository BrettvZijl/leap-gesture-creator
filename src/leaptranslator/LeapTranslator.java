/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package leaptranslator;

import com.leapmotion.leap.Controller;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Brett Van Zijl
 */
public class LeapTranslator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, InterruptedException {
       while (true){
           System.out.println("Press Enter Option:");
           System.out.println("1 to Record a Gesture.");
           System.out.println("2 to Translate Gestures.");
           System.out.println("e to Exit");
           Scanner input = new Scanner(System.in);
           String choice = input.nextLine();
           
           if (choice.equals("1")){
               TrainGesture train = new TrainGesture();
               train.run();
           }else if (choice.equals("2")){
               TranslateGestures translate = new TranslateGestures();
               translate.run();
           }else if (choice.equals("e")){
               break;
           }
       }
    }
    
}
