/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package leaptranslator;

import com.leapmotion.leap.Controller;
import com.leapmotion.leap.Finger;
import com.leapmotion.leap.FingerList;
import com.leapmotion.leap.Frame;
import com.leapmotion.leap.Hand;
import com.leapmotion.leap.HandList;
import com.leapmotion.leap.Listener;
import com.leapmotion.leap.Vector;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Brett Van Zijl
 */
public class Trainer extends Listener{
    Gesture gesture;
    Boolean recording = false; 
    int frameCount = 0;
    int minGestureFrames = 5;	// The minimum number of recorded frames considered as possibly containing a recognisable gesture 
    int minRecordingVelocity = 60; // The minimum velocity a frame needs to clock in at to trigger gesture recording, or below to stop gesture recording (by default)
    int maxRecordingVelocity = 30;	// The maximum velocity a frame can measure at and still trigger pose recording, or above which to stop pose recording (by default)
    Boolean stopRecording = false;  //says if recording should be stopped
    
    public Trainer(String name){
        gesture = new Gesture(name);
    } 
            
    @Override
    public void onConnect(Controller controller) {
        System.out.println("Connected");
        //recording = true;
        //System.out.println("Recording...");
    }

    @Override
    public void onFrame(Controller controller) {
        Frame frame = controller.frame();
        //System.out.println("in frame...");
        
        if (stopRecording){ return;}
        
        
        if (recordableFrame(frame, minRecordingVelocity)){
            
            /*
             * If this is the first frame in a gesture, we clean up some running values and fire the 'started-recording' event.
             */
            if (!recording) { 
                String name = gesture.name;
                gesture = new Gesture(name);
                recording = true; 
                frameCount = 0;
            }
            
            
            //System.out.println("in frame... " + Long.toString(frame.id()));
            recordFrame(frame);
            frameCount++;
            //System.out.println("Recording Frame...");
        }else if(recording){
            /*
             * If the frame should not be recorded but recording was active, then we deactivate recording and check to see if enough 
             * frames have been recorded to qualify for gesture recognition.
             */
            recording = false;
            /*
             * As soon as we're no longer recording, we fire the 'stopped-recording' function.
             */
            
                
            if (frameCount >= minGestureFrames){
                
                try {
                    saveTrainingGesture(gesture);
                    stopRecording=true;
                } catch (IOException ex) {
                    Logger.getLogger(Trainer.class.getName()).log(Level.SEVERE, null, ex);
                }finally{
                    System.out.println("Gesture Saved");
                    try {
                        Robot robot = new Robot();
                        robot.keyPress(KeyEvent.VK_ENTER);
                        robot.keyRelease(KeyEvent.VK_ENTER);
                    } catch (AWTException ex) {
                        Logger.getLogger(Trainer.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
            }
        }
        
        
    }
    
    /**
     * This function is called for each frame during gesture recording, and it is responsible for adding values in frames using the provided 
     * recordVector and recordValue functions (which accept a 3-value numeric array and a single numeric value respectively).
     */
    public void recordFrame(Frame frame) {
        //HandList hands = frame.hands();
        //gesture.hands = hands;
        
        HandList hands = frame.hands();
        int handCount = hands.count();

        Hand hand; 
        Finger finger; 
        FingerList fingers; 
        int fingerCount;
               
        int l = handCount;
        for (int i = 0; i < l; i++) {   //for each hand in the frame
            hand = hands.get(i);
                   
            recordPoint(hand.stabilizedPalmPosition());     //record the palm position

            fingers = hand.fingers();
            fingerCount = fingers.count();
		
            int k = fingerCount;
            for (int j = 0; j < k; j++) {   //for each finger in the hand
                finger = fingers.get(j);
                recordPoint(finger.stabilizedTipPosition());	//record the fingertip position.
            }
        }
        System.out.println("Recording Frame..." + Long.toString(frame.id()));
    }
    
    /**
     * This function records a point to the gesture
     * @param val 
     */
    public void recordPoint(Vector val){
        double x,y,z;
        //NaNs are replaced with 0.0, though they shouldn't occur!
        if (Double.isNaN(val.getX())){
            x=0.0;
        }else{
            x=val.getX();
        }
        
        if (Double.isNaN(val.getY())){
            y=0.0;
        }else{
            y=val.getY();
        }
        
        if (Double.isNaN(val.getZ())){
            z=0.0;
        }else{
            z=val.getZ();
        }
        
        Point point = new Point(x, y, z);
        gesture.add(point);
    }
    
    /**
     * This function returns TRUE if the provided frame should trigger recording and FALSE if it should stop recording.  
     * 
     * Of course, if the system isn't already recording, returning FALSE does nothing, and vice versa.. So really it returns 
     * whether or not a frame may possibly be part of a gesture.
     * 
     * By default this function makes its decision based on one or more hands or fingers in the frame moving faster than the 
     * configured minRecordingVelocity, which is provided as a second parameter.
     * 
     * @param frame
     * @param min
     * @param max
     * @returns {Boolean}
     */
    public Boolean recordableFrame(Frame frame, int min){
        
        HandList hands = frame.hands();
        int j;
        Hand hand;
        FingerList fingers;
        double palmVelocity;
        double tipVelocity;
        Boolean poseRecordable = false;
            
        int l=hands.count();
        for(int i=0; i<l; i++){
            hand= hands.get(i);
                
            Vector palmVelocitys = hand.palmVelocity();
            palmVelocity = Math.max(Math.abs(palmVelocitys.getX()), Math.abs(palmVelocitys.getY()));
            palmVelocity = Math.max(palmVelocity, Math.abs(palmVelocitys.getZ()));
                
            /*
             * We return true if there is a hand moving above the minimum recording velocity
             */
             if (palmVelocity >= min){return true;}
                
             fingers = hand.fingers();
                
             int k = fingers.count();
             for (j=0; j<k; j++){
                 Vector tipVelocitys = fingers.get(j).tipVelocity();
                 tipVelocity = Math.max(Math.abs(tipVelocitys.getX()), Math.abs(tipVelocitys.getY()));
                 tipVelocity = Math.max(tipVelocity, Math.abs(tipVelocitys.getZ()));
                    
                /*
                 * Or if there's a finger tip moving above the minimum recording velocity
                 */
                if (tipVelocity >= min) { return true; }
            }
        }
            
            
        return false;
    }
    /**
     * Save the recorded gesture to an external file (with the same name as the gesture)
     * for later use.
     * 
     * @param gestureIn
     */
    public void saveTrainingGesture(Gesture gestureIn) throws IOException{
        String fileName= "Gestures\\"+gestureIn.name;
        ObjectOutputStream out = null;
        
        try{
            System.out.println("Saving Gesture...");
            out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(fileName)));
            out.writeObject(gestureIn);
        } catch (IOException ex) {
            Logger.getLogger(Trainer.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }
    
    /**
     * Set the stopRecording variable to true, to tell the main program that the
     * gesture has been recorded and so no longer needs to record anymore gestures.
     * 
     */
    public void stopRecording(){
        stopRecording=true;
    }
    
}
