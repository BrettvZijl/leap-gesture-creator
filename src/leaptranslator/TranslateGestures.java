/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package leaptranslator;

import com.leapmotion.leap.Controller;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Brett Van Zijl
 */
public class TranslateGestures {
    public void run() throws IOException, InterruptedException{
       Recognizer listener = new Recognizer();
       Controller controller = new Controller();
       controller.setPolicy(Controller.PolicyFlag.POLICY_OPTIMIZE_HMD); //optimized for head mounted display (LM being in vertical orientation)
       controller.addListener(listener);
       
     /* while (!listener.stopRecording){
          //System.out.println("Recording..." + listener.stopRecording.toString());
       }*/
        
       // Keep this process running until Enter is pressed
       //This is the only method that I could find that allows the process to run without causing issues. while(true) statements did not work.
        try {
           System.in.read();
       } catch (IOException e) {
           e.printStackTrace();
       }
        
       controller.removeListener(listener);
       System.out.println("Removed Listener");
       
    }
}
