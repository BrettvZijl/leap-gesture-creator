/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package leaptranslator;

import java.util.ArrayList;



/**
 *
 * @author Brett Van Zijl
 */
class TemplateMatcher {
    int n = 44; //number of points      // Gestures are resampled to this number of points
    Point origin;  // Gestures are translated to be centered on this point
    
    public TemplateMatcher(){
        origin = new Point(0.0,0.0,0.0);
    }
    
    /**
     * This is the primary correlation function, called in order to compare a detected 
     * gesture with known gestures.  
     * 
     * @param gesture
     * @param templates
     * @returns
     */
    public RecognizerResults recognizer(Gesture gesture, ArrayList<Gesture> templates){
        //int n = 32; //number of points
        Gesture result= null;
        normalize(gesture, n);
        double score = Double.POSITIVE_INFINITY;
        for(Gesture template: templates){
            //normalize(template, n); //should be pre-processed
            double d = greedyCloudMatch(gesture, template);
            if (score > d){
                score = d;
                result = template;
            }
        }
        if (result == null){
            return new RecognizerResults("No Match", 0.0);
        }
        
        score = Math.max((score - 3.0) / 3.0, 0.0);
        return new RecognizerResults(result.name, score);
    } 
    
    /**
     * Resamples a gesture in order to create gestures of homogenous lengths.  The second parameter indicates the length to 
     * which to resample the gesture.
     * 
     * This function is used to homogenize the lengths of gestures, in order to make them more comparable. 
     * 
     * @param gesture
     * @param n
     * @returns {Array}
     */
    public ArrayList<Point> resample(ArrayList<Point> points, int n){
        double I = pathLength(points)/n-1;
        double D = 0.0;
        ArrayList<Point> newPoints = new ArrayList<Point>(); 
	newPoints.add(points.get(0));
        
        for( int i = 1; i < points.size(); i++ ){
            //System.out.println("Operating "+ Integer.toString(i));
            if( points.get(i).stroke == points.get(i-1).stroke){
                double d = euclideanDistance(points.get(i-1), points.get(i));
                if ((D+d)>= I){
                    double qx = points.get(i-1).x + ((I - D)/d) * (points.get(i).x - points.get(i-1).x);
                    double qy = points.get(i-1).y + ((I - D)/d) * (points.get(i).y - points.get(i-1).y);
                    double qz = points.get(i-1).z + ((I - D)/d) * (points.get(i).z - points.get(i-1).z);
                    
                    Point q = new Point(qx, qy, qz, points.get(i).stroke);
                    newPoints.add(q); // append new point q
                    points.add(i, q); // insert 'q' at position i in points so that q will be the next i
                    D = 0.0;
                }else{
                    D += d;
                }
            }
        }
        
        // sometimes we fall a rounding-error short of adding the last point, so add it if so
	if(newPoints.size() == n - 1){ 
            newPoints.add(new Point(points.get(points.size() - 1).x, points.get(points.size() - 1).y, points.get(points.size() - 1).z, points.get(points.size() - 1).stroke));
        }    
        return newPoints;
    }
    
    /**
     * Scales gestures to homogenous variances in order to provide for detection of the same gesture at different scales.
     * 
     * @param gesture
     * @returns {Array}
     */
    public ArrayList<Point> scale(ArrayList<Point> points){
        double minX = Double.POSITIVE_INFINITY;
        double maxX = Double.NEGATIVE_INFINITY;
	double minY = Double.POSITIVE_INFINITY;
        double maxY = Double.NEGATIVE_INFINITY;
        double minZ = Double.POSITIVE_INFINITY;
	double maxZ = Double.NEGATIVE_INFINITY;
        
        for(int i = 0; i < points.size(); i++) {    //for each point
            minX = Math.min(minX, points.get(i).x);
            minY = Math.min(minY, points.get(i).y);
            minZ = Math.min(minZ, points.get(i).z);
            maxX = Math.max(maxX, points.get(i).x);
            maxY = Math.max(maxY, points.get(i).y);
            maxZ = Math.max(maxZ, points.get(i).z);
        }
        double size = Math.max(maxX - minX, Math.max((maxY - minY), (maxZ-minZ)));
	ArrayList<Point> newpoints = new ArrayList<Point>();

	for(int i = 0; i < points.size(); i++){
            double qx = (points.get(i).x - minX) / size;
            double qy = (points.get(i).y - minY) / size;
            double qz = (points.get(i).z - minZ) / size;
            newpoints.add(new Point(qx, qy, qz, points.get(i).stroke));
	}
	return newpoints;
    }
    
    /**
     * Translates a gesture to the provided centroid.  This function is used to map all gestures to the 
     * origin, in order to recognize gestures that are the same, but occurring at at different point in space.
     * 
     * @param gesture
     * @param centroid
     * @returns {Array}
     */
    public ArrayList<Point> translateTo(ArrayList<Point> points, Point centroid){
        Point c = centroid(points);
        ArrayList<Point> newPoints = new ArrayList<Point>();
        
        for (int i=0; i < points.size(); i++){
            double qx = points.get(i).x + centroid.x - c.x;
            double qy = points.get(i).y + centroid.y - c.y;
            double qz = points.get(i).z + centroid.z - c.z;
            newPoints.add(new Point(qx, qy, qz, points.get(i).stroke));
        }
        
        return newPoints;
    }
    
    /**
     * Finds the center of a gesture by averaging the X, Y and Z coordinates of all points in the gesture data.
     * 
     * @param gesture
     * @returns {Point}
     */
    public Point centroid(ArrayList<Point> points){
        double x = 0.0;
        double y = 0.0;
        double z = 0.0;
        
        for(int i = 0; i < points.size(); i++){
            x += points.get(i).x;
            y += points.get(i).y;
            z += points.get(i).z;
	}
        
        x /= points.size();
	y /= points.size();
        z /= points.size();
        
	return new Point(x, y, z, 0);
    }
    
    /**
     * Calculates the length traversed by a single point in a gesture
     * 
     * @param gesture
     * @returns {Number}
     */
    public double pathLength(ArrayList<Point> points){
        double d = 0.0;
        for (int i=1; i<points.size(); i++){
            if (points.get(i).stroke == points.get(i-1).stroke){
                d += euclideanDistance(points.get(i-1), points.get(i));
            }
        }
        
        return d;
    }
    
    /**
     * Prepares a recorded gesture for template matching - resampling, scaling, and translating the gesture to the 
     * origin.  Gesture processing ensures that during recognition, apples are compared to apples - all gestures are the 
     * same (resampled) length, have the same scale, and share a centroid.
     * 
     * @param gesture
     * @returns
     */
    private void normalize(Gesture gesture, int n) {
        gesture.points = resample(gesture.points, n);
        gesture.points = scale(gesture.points);
        gesture.points = translateTo(gesture.points, origin);
        
    }

    /**
     * Match two gestures (gesture and template by performing repeated alignments between their points.
     * Each new alignment starts with a different starting point index i.
     * 
     * @param gesture
     * @param template
     * @return 
     */
    private double greedyCloudMatch(Gesture gesture, Gesture template) {
        ArrayList<Point> gPoints = gesture.points;
        ArrayList<Point> tPoints = template.points;
        double e = 0.50;
	double step = Math.floor(Math.pow(gPoints.size(), 1 - e));

	double min = Double.POSITIVE_INFINITY;
	for( int i = 0; i < gPoints.size(); i += step )
	{
            double d1 = cloudDistance(gPoints, tPoints, i);
            double d2 = cloudDistance(tPoints, gPoints, i);
            min = Math.min(min, Math.min(d1, d2)); // min3
	}
	return min;
    }

    /**
     * Distance between two point clouds. Computes the minimum cost alignment between
     * points1 and points 2 starting with point start.
     * Assigns decreasing confidence weights to point matchings.
     * 
     * @param points1
     * @param points2
     * @param start
     * @return 
     */
    private double cloudDistance(ArrayList<Point> points1, ArrayList<Point> points2, int start) {
        // pts1.size() == pts2.size()
	boolean[] matched = new boolean[points1.size()]; 
	for( int k = 0; k < points1.size(); k++ ){
            matched[k] = false;
        }
	double sum = 0;
	int i = start; //start matching with points i
	do
	{
            int index = -1;
            double min = Double.POSITIVE_INFINITY;
            for(int j = 0; j < matched.length; j++){
                if(!matched[j]) {
                    double d = euclideanDistance(points1.get(i), points2.get(j));
                    if(d < min) {
                        min = d;
                        index = j;
                    }
                }
            }
            matched[index] = true;
            double weight = 1 - ((i - start + points1.size()) % points1.size()) / points1.size();
            sum += weight * min;
            i = (i + 1) % points1.size();
	} while( i != start ); //all points are processed
	return sum;
    }
    
    /**
     * A simple Euclidean distance function
     * 
     * @param p1
     * @param p2
     * @returns
     */
    private static double euclideanDistance(Point p1, Point p2)
    {
        double dx = p1.x - p2.x;
	double dy = p1.y - p2.y;
	double dz = p1.z - p2.z;

	return Math.sqrt(dx * dx + dy * dy + dz * dz);
    }	
}