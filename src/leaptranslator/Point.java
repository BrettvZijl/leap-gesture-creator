/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package leaptranslator;

import java.io.Serializable;

/**
 *
 * @author Brett Van Zijl
 * This class stores a Point in the x,y,z 3D plane.
 */
public class Point implements Serializable{
    public double x;
    public double y;
    public double z;
    public int stroke;
          
    public Point(double xIn, double yIn, double zIn, int strokeIn) {
         x = xIn;
         y = yIn;
         z = zIn;
         stroke = strokeIn; // stroke ID to which this point belongs (1,2,...)
    }
            
    public Point(double xIn, double yIn, double zIn) {
         x = xIn;
         y = yIn;
         z = zIn;
         stroke = 0; // stroke ID to which this point belongs (1,2,...)
    }
 }

