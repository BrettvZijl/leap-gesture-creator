/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package leaptranslator;

/**
 *
 * @author Brett Van Zijl
 */
public class RecognizerResults {
    public String mName;
    public double mScore;
    public String mOtherInfo;

    public RecognizerResults(String name, double score){ 
        mName = name; 
        mScore = score; 
    }
    
    public RecognizerResults(String name, double score, String otherInfo){ 
        mName = name; mScore = score; 
        mOtherInfo = otherInfo; 
    }
}
